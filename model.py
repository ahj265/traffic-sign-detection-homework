import torch
import torch.nn as nn
import torch.nn.functional as F

class SELayer(nn.Module):
    def __init__(self, channel, reduction=16):
        super(SELayer, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc = nn.Sequential(
            nn.Linear(channel, channel // reduction, bias=False),
            nn.ReLU(inplace=True),
            nn.Linear(channel // reduction, channel, bias=False),
            nn.Sigmoid()
        )

    def forward(self, x):
        b, c, _, _ = x.size()
        y = self.avg_pool(x).view(b, c)
        y = self.fc(y).view(b, c, 1, 1)
        return x * y.expand_as(x)

class STN(nn.Module):
    def __init__(self, in_channels, num_feat1, num_feat2, loc_net_feats, final_spatial_dim):
        super(STN, self).__init__()

        self.num_feat2 = num_feat2

        self.conv1 = nn.Conv2d(in_channels, num_feat1, kernel_size=7)
        self.conv2 = nn.Conv2d(num_feat1, num_feat2, kernel_size=5)

        self.loc_net = nn.Sequential(
            nn.Linear(num_feat2 * final_spatial_dim * final_spatial_dim, loc_net_feats),
            nn.ReLU(True),
            nn.Linear(loc_net_feats, 6)
        )

        # initialize loc's last layer
        self.loc_net[2].weight.data.zero_()
        self.loc_net[2].bias.data.copy_(torch.tensor([1, 0, 0, 0, 1, 0], dtype=torch.float))

    def forward(self, x):
        xs = F.relu(F.max_pool2d(self.conv1(x), 2))
        xs = F.relu(F.max_pool2d(self.conv2(xs), 2))
        xs = xs.view(-1, self.num_feat2 * xs.size(2) * xs.size(3))

        theta = self.loc_net(xs)
        theta = theta.view(-1, 2, 3)
        grid = F.affine_grid(theta, x.size())
        x = F.grid_sample(x, grid)

        return x

class Net(nn.Module):
    def __init__(self, args):
        super(Net, self).__init__()

        self.args = args

        self.stn1 = STN(
            in_channels=3,
            num_feat1=8,
            num_feat2=10,
            loc_net_feats=32,
            final_spatial_dim=4
        )
        self.conv1 = nn.Conv2d(3, self.args.layer_1_channels, kernel_size=5)
        self.bn1 = nn.BatchNorm2d(self.args.layer_1_channels)

        self.conv2 = nn.Conv2d(self.args.layer_1_channels, self.args.layer_2_channels, kernel_size=3)
        self.bn2 = nn.BatchNorm2d(self.args.layer_2_channels)

        self.conv3 = nn.Conv2d(self.args.layer_2_channels, self.args.layer_3_channels, kernel_size=3)
        self.bn3 = nn.BatchNorm2d(self.args.layer_3_channels)

        self.fc1 = nn.Linear(self.args.layer_3_channels * 2 * 2, self.args.linear_layer)
        self.fc2 = nn.Linear(self.args.linear_layer, self.args.nclasses)

        self.conv_drop = nn.Dropout2d(p=0.5)
        self.linear_drop = nn.Dropout(p=0.5)

        if self.args.se1:
            self.se1 = SELayer(self.args.layer_1_channels, 10)

        if self.args.se2:
            self.se2 = SELayer(self.args.layer_2_channels, 10)

        if self.args.se3:
            self.se3 = SELayer(self.args.layer_3_channels, 10)

    def forward(self, x):
        x = self.stn1(x)
        x = self.bn1(F.max_pool2d(F.leaky_relu(self.conv1(x)), 2))

        if self.args.se1:
            x = self.se1(x)
        else:
            x = self.conv_drop(x)

        x = self.bn2(F.max_pool2d(F.leaky_relu(self.conv2(x)), 2))

        if self.args.se2:
            x = self.se2(x)
        else:
            x = self.conv_drop(x)

        x = self.bn3(F.max_pool2d(F.leaky_relu(self.conv3(x)), 2))

        if self.args.se3:
            x = self.se3(x)
        else:
            x = self.conv_drop(x)

        x = x.view(-1, self.args.layer_3_channels * 2 * 2)
        x = self.linear_drop(F.relu(self.fc1(x)))

        return F.log_softmax(self.fc2(x), dim=1)
