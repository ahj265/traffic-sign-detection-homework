from __future__ import print_function
import argparse
import torch
import os
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable

from data import initialize_data, data_transforms, data_transforms_list
from data import data_center, data_hvflip

from model import Net

import pytorch_lightning as pl
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

from lookahead import Lookahead
from radam import RAdam

# Training settings
parser = argparse.ArgumentParser(description='PyTorch GTSRB example')

parser.add_argument('--exp_version', type=int, default=0,
                    help='Helpful in resuming experiments.')
parser.add_argument('--data', type=str, default='data', metavar='D',
                    help="folder where data is located. train_data.zip and test_data.zip need to be found in the folder")
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='input batch size for training (default: 64)')
parser.add_argument('--epochs', type=int, default=100, metavar='N',
                    help='number of epochs to train (default: 10)')
parser.add_argument('--lr', type=float, default=0.0001, metavar='LR',
                    help='learning rate (default: 0.01)')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--log-interval', type=int, default=1000, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--worker', type=int, default=2,
                    help='number of dataloader threads')
parser.add_argument('--save-folder', type=str, default="save/", help="save folder")

parser.add_argument('--layer-1-channels', type=int, default=100, help="number of channels in 1st layer")
parser.add_argument('--layer-2-channels', type=int, default=150, help="number of channels in 2nd layer")
parser.add_argument('--layer-3-channels', type=int, default=250, help="number of channels in 3rd layer")
parser.add_argument('--linear-layer', type=int, default=350, help="number of nodes in the linear layer")
parser.add_argument('--nclasses', type=int, default=43, help="number of classes in the dataset")

parser.add_argument('--se1', action='store_true', default=False, help="add se1")
parser.add_argument('--se2', action='store_true', default=False, help="add se2")
parser.add_argument('--se3', action='store_true', default=False, help="add se3")

parser.add_argument('--opt', type=str, default='adam', help="select between adam/radam")
parser.add_argument('--lookahead', action='store_true', default=False, help="use lookahead")
args = parser.parse_args()

torch.manual_seed(args.seed)

### Data Initialization and Loading # data.py in the same folder
initialize_data(args.data) # extracts the zip files, makes a validation set

class GTSRBTrainer(pl.LightningModule):
    def __init__(self, args):
        super(GTSRBTrainer, self).__init__()

        self.args = args
        self.model = Net(args)

    def forward(self, x):
        return self.model(x)

    def training_step(self, batch, batch_nb):
        data, target = batch

        output = self.forward(data)
        loss = F.nll_loss(output, target)

        tensorboard_logs = {'train_loss': loss}

        return {'loss': loss, 'log': tensorboard_logs}

    def validation_step(self, batch, batch_nb):
        data, target = batch

        output = self.forward(data)
        labels_hat = torch.argmax(output, dim=1)
        val_acc = torch.sum(target == labels_hat).item() / (len(target) * 1.0)

        return {
            'val_loss': F.nll_loss(output, target),
            'val_acc': torch.tensor(val_acc)
        }

    def validation_end(self, outputs):
        val_loss_mean = 0
        val_acc_mean = 0

        for output in outputs:
            val_loss_mean += output['val_loss']
            val_acc_mean += output['val_acc']

        val_loss_mean /= len(outputs)
        val_acc_mean /= len(outputs)

        tensorboard_logs = {'val_loss': val_loss_mean, 'val_acc': val_acc_mean}

        return {
            'val_loss': val_loss_mean,
            'val_acc': val_acc_mean,
            'log': tensorboard_logs,
            'progress_bar': tensorboard_logs
        }

    def configure_optimizers(self):
        if self.args.opt == 'adam':
            optimizer = torch.optim.Adam(
                filter(lambda p: p.requires_grad, self.model.parameters()),
                lr=self.args.lr, betas=(0.9, 0.999)
            )
        elif self.args.opt == 'radam':
            optimizer = RAdam(
                filter(lambda p: p.requires_grad, self.model.parameters()),
                lr=self.args.lr, betas=(0.9, 0.999)
            )

        if self.args.lookahead:
            lookahead = Lookahead(optimizer, k=5, alpha=0.5)

        if self.args.lookahead:
            lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(
                lookahead, 'min', patience=10, factor=0.5, verbose=True
            )
        else:
            lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(
                optimizer, 'min', patience=10, factor=0.5, verbose=True
            )

        if self.args.lookahead:
            return [lookahead], [lr_scheduler]
        else:
            return [optimizer], [lr_scheduler]

    @pl.data_loader
    def train_dataloader(self):
        concat_datasets = []

        concat_datasets.append(datasets.ImageFolder(args.data + '/train_images', transform=data_transforms))
        concat_datasets.append(datasets.ImageFolder(args.data + '/train_images', transform=data_hvflip))
        concat_datasets.append(datasets.ImageFolder(args.data + '/train_images', transform=data_center))

        for data_transform in data_transforms_list:
            concat_datasets.append(datasets.ImageFolder(args.data + '/train_images', transform=data_transform))

        return torch.utils.data.DataLoader(
            torch.utils.data.ConcatDataset(concat_datasets),
            batch_size=args.batch_size,
            shuffle=True,
            num_workers=args.worker
        )

    @pl.data_loader
    def val_dataloader(self):
        return torch.utils.data.DataLoader(
            datasets.ImageFolder(args.data + '/val_images', transform=data_transforms),
            batch_size=args.batch_size, shuffle=False, num_workers=args.worker
        )

def main():
    # ------------------------
    # 1 INIT LIGHTNING MODEL
    # ------------------------
    print('loading model...')
    model = GTSRBTrainer(args)
    print('model built')

    # ------------------------
    # 2 DEFINE CALLBACKS
    # ------------------------
    checkpoint_callback = ModelCheckpoint(
        filepath=args.save_folder,
        save_best_only=True,
        verbose=True,
        monitor='val_loss',
        mode='min',
        prefix=''
    )

    # ------------------------
    # 3 INIT TRAINER
    # ------------------------
    trainer = Trainer(
        gpus=1,
        early_stop_callback=None,
        checkpoint_callback=checkpoint_callback,
        max_nb_epochs=args.epochs,
        show_progress_bar=True
    )

    # ------------------------
    # 4 START TRAINING
    # ------------------------
    trainer.fit(model)

if __name__ == '__main__':
    main()
