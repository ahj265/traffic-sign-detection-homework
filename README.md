#### Libraries used

- pytorch, >= 1.2.0
- pytorch-lightning, >= 0.5.1
- pandas
- numpy

Code for lookahead and RAdam borrowed from the following implementations.
- https://github.com/LiyuanLucasLiu/RAdam
- https://github.com/alphadl/lookahead.pytorch

#### Training and validation

- train using python main.py and using one of the options mentioned in argument section of main.py
- main.py trains the model and checks for validation performance after each epoch
- generate results for uploading to kaggle by using evaluate.py
- evaluate.py also needs the same arguments passed to the model during training