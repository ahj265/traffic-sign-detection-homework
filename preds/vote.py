import pandas
import os
import pandas as pd

from collections import Counter

# collect all preds file in this folder
files = [f for f in os.listdir('.') if os.path.isfile(f)]
# folter out names of other files
files = [f for f in files if '_pred.csv' not in f]
files = [f for f in files if '.DS_Store' not in f]
files = [f for f in files if 'vote.py' not in f]

df = pd.concat([pd.read_csv(file_name, dtype={'Filename': str}).sort_values('Filename').reset_index() for file_name in files], axis=1)
df["counter"] = df.ClassId.apply(lambda x: Counter(x), axis=1)
df["vote"] = df.counter.apply(lambda x: Counter(x).most_common()[0])

df_out = pd.read_csv("pred_1.csv", dtype={'Filename': str}).sort_values('Filename').reset_index()
df_out["ClassId"] = df.vote.apply(lambda x: x[0])
df_out[["Filename", "ClassId"]].to_csv("__pred.csv", index=None)
