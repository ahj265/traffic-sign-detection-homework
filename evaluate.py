from __future__ import print_function
import argparse
from tqdm import tqdm
import os
import PIL.Image as Image

import torch
from torch.autograd import Variable
import torch.nn.functional as F
import torchvision.datasets as datasets

from data import initialize_data # data.py in the same folder
from model import Net

parser = argparse.ArgumentParser(description='PyTorch GTSRB evaluation script')
parser.add_argument('--data', type=str, default='data', metavar='D',
                    help="folder where data is located. train_data.zip and test_data.zip need to be found in the folder")
parser.add_argument('--model', type=str, metavar='M',
                    help="the model file to be evaluated. Usually it is of the form model_X.pth")
parser.add_argument('--outfile', type=str, default='gtsrb_kaggle.csv', metavar='D',
                    help="name of the output csv file")

parser.add_argument('--layer-1-channels', type=int, default=100, help="number of channels in 1st layer")
parser.add_argument('--layer-2-channels', type=int, default=150, help="number of channels in 2nd layer")
parser.add_argument('--layer-3-channels', type=int, default=250, help="number of channels in 3rd layer")
parser.add_argument('--linear-layer', type=int, default=350, help="number of nodes in the linear layer")
parser.add_argument('--nclasses', type=int, default=43, help="number of classes in the dataset")

parser.add_argument('--se1', action='store_true', default=False, help="add se1")
parser.add_argument('--se2', action='store_true', default=False, help="add se2")
parser.add_argument('--se3', action='store_true', default=False, help="add se3")

args = parser.parse_args()

# need to remove 'model.' from weight keys, as models trained by lightning append that
state_dict = torch.load(args.model)['state_dict']

mod_state_dict = {}
for key in state_dict.keys():
    mod_state_dict[key.replace('model.', '')] = state_dict[key]

model = Net(args)
model.load_state_dict(mod_state_dict)
model.eval()

from data import data_transforms

test_dir = args.data + '/test_images'

def pil_loader(path):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        with Image.open(f) as img:
            return img.convert('RGB')


output_file = open(args.outfile, "w")
output_file.write("Filename,ClassId\n")
for f in tqdm(os.listdir(test_dir)):
    if 'ppm' in f:
        data = data_transforms(pil_loader(test_dir + '/' + f))
        data = data.view(1, data.size(0), data.size(1), data.size(2))
        data = Variable(data, volatile=True)
        output = model(data)
        pred = output.data.max(1, keepdim=True)[1]

        file_id = f[0:5]
        output_file.write("%s,%d\n" % (file_id, pred))

output_file.close()

print("Succesfully wrote " + args.outfile + ', you can upload this file to the kaggle '
      'competition at https://www.kaggle.com/c/nyucvfall2019/')
        


